# Sexy Artsey

My spin on Artsey.io keyboard based on Seeed Studio Xiao (BLE or RP2040). Aimed to be used as a portable wireless keyboard for the phone but can also be used as a macropad or a Work Louder keyboard module.

## Features:

- 10 keys, can be flipped to any side
- Choc V1 and MX switches support (MX under encoder only supports one direction)
- Optional encoder support (replaces two keys)
- Optional OLED support (replaces one key)
- Supports Seeed Studio Xiao (BLE or RP2040) and WaveShare RP2040 Zero controllers

The purpose for the display is to enable some kind of autocomplete but not sure how to do that yet.
